#include <define.h>
#include <controlboard.h>

int GetGwbScInfo(GWB_SC_INFO_T* pGwbScInfo)
{
	pGwbScInfo -> fBoardTemp        = 0;

	pGwbScInfo -> cBoardTempResult  = 0;
	pGwbScInfo -> cBoardVolt        = 0;
	pGwbScInfo -> c422Channel       = 0;
	pGwbScInfo -> s429RecvChannel   = 0;
	pGwbScInfo -> c429SendChannel   = 0;
	pGwbScInfo -> cDiscreteInput    = 0;

	return 0;
}

int GetTwbScInfo(TWB_SC_INFO_T* pTwbScInfo)
{
	return 0;
}

int GetLeftEgpwpScInfo(EGPWP_SC_INFO_T* pEgpwpScInfo)
{
	return 0;
}


int GetRightEgpwpScInfo(EGPWP_SC_INFO_T* pEgpwpScInfo)
{
	return 0;
}

int GetInInfo(IN_INFO_T* pInInfo)
{
	return 0;
}

int GetAirInfo(AIR_INFO_T* pAirInfo)
{
	return 0;
}

int GetRaltInfo(RALT_INFO_T* pRaltInfo)
{
	return 0;
}

int GetMlInfo(ML_INFO_T* pMlInfo)
{
	return 0;
}

int GetIlInfo(IL_INFO_T* pIlInfo)
{
	return 0;
}

int GetFrontLgpInfo(LGP_INFO_U* pLgpInfo)
{
	return 0;
}

int GetLeftLgpInfo(LGP_INFO_U* pLgpInfo)
{
	return 0;
}

int GetRightLgpInfo(LGP_INFO_U* pLgpInfo)
{
	return 0;
}

int GetFlapInfo(FLAP_INFO_U* pFlapInfo)
{
	return 0;
}

int GetWhldInfo(WHLD_INFO_U* pWhldInfo)
{
	return 0;
}

int GetHorizonInfo(HORIZON_INFO_T* pHorizonInfo)
{
	return 0;
}

int GetOlmtInfo(OLMT_INFO_T* pOlmtInfo)
{
	return 0;
}

int GetMountInfo(MOUNT_INFO_T* pMountInfo)
{
	return 0;
}

void RegKeyOpCallBack(KeyOpCallBackFuc* pFunc)
{
	EGPWP_KEY_INFO_T key_info;

	/* Get Key info from serial port and put into struct */
	/* TODO: */
	(*pFunc)(&key_info);
}

int SendLightInfo(EGPWP_KEY_INFO_T tKeyInfo)
{
	send_packet(MSG_LIGHT, 0, &tKeyInfo, sizeof(tKeyInfo));
	return 0;
}


void RegBeforeDmdCallBack(DmdBeforeCallBackFuc* pFunc)
{
	DMD_STATUS_SWITCH_U dmd_status;
	(*pFunc) (&dmd_status);
}


void RegAfterDmdCallBack(AfterDmdCallBackFuc* pFunc)
{
	TW_UPBACK_T TwUpbackInfo;
	GW_UPBACK_T GwUpbackInfo;
	(*pFunc) (&TwUpbackInfo, &GwUpbackInfo);
}

void SendDmdStatusSwitch(DMD_STATUS_SWITCH_U uDmdStatusSwitch)
{
	/* TODO: */
}

LOCAL_TIME_T   local_time;
int GetLocalTime(LOCAL_TIME_T* pLocalTime)
{
	/* TODO: */
//	pLocalTime -> uYear = 0;
	memcpy (pLocalTime, &local_time, sizeof(LOCAL_TIME_T));
}
