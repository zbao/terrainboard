#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <controlboard.h>
#include <string.h>

extern int uart_fd;
unsigned char rcv_buf[512] = {0};
unsigned char gfilename[32] = {0};

int get_packet()
{
	CONTROL_BOARD_MESSAGE_HEAD_T *phead = (CONTROL_BOARD_MESSAGE_HEAD_T *)rcv_buf;
	do {
		UART0_Recv(uart_fd, rcv_buf, sizeof(int));
	} while (phead->magic == 0x5a5a5a5a);
	UART0_Recv(uart_fd, &(phead->length), sizeof(short));
	UART0_Recv(uart_fd, &(phead->src_board), phead->length);
}

void response_packet(unsigned char msg, unsigned char dst_board, unsigned data)
{
	CONTROL_BOARD_MESSAGE_HEAD_T packet;

	packet.magic = 0x5a5a5a5a;
	packet.length = 4;
	packet.dst_board = dst_board;	/* TODO: */
	packet.src_board = 0x10;
	packet.msg = msg;
	packet.data[0] = data;

	UART0_Send(uart_fd, packet, sizeof packet);
}

void send_packet(unsigned char msg, unsigned char dst_board, unsigned char *data, unsigned int length)
{
	CONTROL_BOARD_MESSAGE_HEAD_T packet;

	packet.magic = 0x5a5a5a5a;
	packet.length = 0;	/* TODO */
	packet.dst_board = dst_board; /* l or  right? */
	packet.src_board = 0x10;
	packet.msg = msg;
	memcpy(packet.data, data, length);

	UART0_Send(uart_fd, packet, length + 3); /* TODO: length? */
}

void savefile(CONTROL_BOARD_MESSAGE_HEAD_T *phead, char *filename)
{
	int fd;

	fd = open (filename, O_APPEND | O_CREAT | O_WRONLY, 0664);
	printf("ssssslength=%d\n", phead->length);
	write (fd, &phead->data[0], phead->length-3);
	close(fd);
}

void decode_packet(CONTROL_BOARD_MESSAGE_HEAD_T *phead)
{
	/* TODO: */
	switch (phead->msg) {
	case MSG_SINGLE_KEY:
	case MSG_LONGPRESS_KEY:
	case MSG_MULTIPLE_KEY:
		printf("get key=%x\n", phead->data[0]);
		/* get key */
		response_packet(MSG_KEY_RESP, phead->src_board, 0); /* return  ok. */
		break;

	case MSG_FLASHDISK_PLUG:
	case MSG_FLASHDISK_UNPLUG:
		printf("(un)plug\n");
		response_packet(phead->msg+1, phead->src_board, 0); /* return ok. */
		/* response */
		break;
	case MSG_FILENAME:
		memcpy(gfilename, phead->data, 32);
		printf("file=%s\n", gfilename);
		response_packet(phead->msg + 1, phead->src_board, 0); /* return ok. */
		/* response */
		break;

	case MSG_FILE_DATA:
	case MSG_FILE_DATA_LAST:
		/* phead->data */
		savefile(phead, gfilename);
		/* phead->length - 6 */
		/* Save data to file */
		/* response */
		break;
	case MSG_LIGHT_RESP:
		break;
	}
}

int control_keys()
{
	get_packet(rcv_buf);
	return 0;
}
