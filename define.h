#ifndef __DEFINE_H__
#define __DEFINE_H__

//告警处理板自检结果(包含上电、维护、周期自检)
typedef struct GWB_SC_INFO_T
{
	//板卡温度
	float fBoardTemp;
	//0:异常，1:正常，2:无效
	unsigned char cBoardTempResult;

	//电压(0: 异常，1：正常)
	unsigned char cBoardVolt;

	//422各路（1路）回环测试（从最低位依次开始表示0~7路，0：异常，1：正常）
	unsigned char c422Channel;

	//429各路（11路）接收回环测试（从最低位依次开始表示0~15路，0：异常，1：正常）
	short s429RecvChannel;

	//429各路（3路）发送回环测试（从最低位依次开始表示0~7路，0：异常，1：正常）
	unsigned char c429SendChannel;

	//8路离散量输入测试（从最低位依次开始表示0~7路，0：异常，1：正常）
	unsigned char cDiscreteInput;
}GWB_SC_INFO_T;

//地形处理板自检结果(包含上电、维护、周期自检)
typedef struct TWB_SC_INFO_T
{
	//CPU温度
	float fCpuTemp;
	//0：异常,1：正常,2：无效
	unsigned char cCpuTempResult;
    
//电压(0：异常, 1：正常)
	unsigned char cBoardVolt;

//422各路（4路）测试（从最低位依次开始表示0~7路，0：异常，1：正常）
	unsigned char c422Channel;

//429各路（11路）接收测试（从最低位依次开始表示0~15路，0：异常，1：正常）
	short s429RecvChannel;

//喇叭测试（0：异常，1：正常）
	unsigned char cVoice;
}TWB_SC_INFO_T;

//近地告警控制板自检结果(包含上电、维护、周期自检)
typedef struct EGPWP_SC_INFO_T
{
//422状态（1路）测试（从最低位依次开始表示0-7路， 0：异常，1：正常）
	unsigned char c422Channel;
}EGPWP_SC_INFO_T;

//惯导数据 （时间是否使用待定）
typedef struct IN_INFO_T
{
//时间 转换为ms (64位)
	unsigned long long ullTime;
	//0代表无效,1代表有效
	char cTValid;

	//经度 单位：°,东经为正,西经为负,范围是-180~180
	double fLongitude;
	//0代表无效,1代表有效
	char cLoValid;

	//纬度 单位：°,北纬为正,南纬为负,范围是-90~90
	double fLatitude;
	//0代表无效,1代表有效
	char cLaValid;

	//高度 单位：m,海平面上为正，海平面下为负
	double fAltitude;
	//0代表无效,1代表有效
	char cAValid;

	//东速 单位：m/s,向东为正，向西为负
	double fEVelocity;
	//0代表无效,1代表有效
	char cEValid;

	//北速 单位：m/s,向北为正，向南为负
	double fNVelocity;
	//0代表无效,1代表有效
	char cNValid;

	//天速 单位：m/s,向上为正，向下为负
	double fHVelocity;
	//0代表无效,1代表有效
	char cHValid;

	//东加速度 单位：m/s2,向东为正，向西为负
	double fEAcceleration;
	//0代表无效,1代表有效
	char cEAValid;

	//北加速度 单位：m/s2,向北为正，向南为负
	double fNAcceleration;
	//0代表无效,1代表有效
	char cNAValid;

	//天加速度 单位：m/s2,向上为正，向下为负
	double fHAcceleration;
	//0代表无效,1代表有效
	char cHAValid;

	//航向 单位：°，相对于真北顺时针为正，[0,360) 
	double fHDG;
	//0代表无效,1代表有效
	char cHDGValid;

	//横滚 单位：°，绕纵轴轴向顺时针转动为正，逆时针转动为负[-180～180]°
	double fRollAngle;
	//0代表无效,1代表有效
	char cRAValid;

	//俯仰 单位：°，水平面之上为正，水平面之下为负[-90～90]°
	double fPitchAngle;
	//0代表无效,1代表有效
	char cPAValid;

	//真空速 单位：m/s
	double fTAS;
	//0代表无效,1代表有效
	char cTASValid;

	//地速 单位：m/s
	double fSOG;
	//0代表无效,1代表有效
	char cSOGValid;
}IN_INFO_T;

//大气测量数据（时间是否使用待定）
typedef struct AIR_INFO_T
{
	//时间 转换为ms
	unsigned long long ullTime;
	//0代表无效,1代表有效
	char cTValid;

	//高度 单位：m,海平面上为正，海平面下为负
	double fAltitude;
	//0代表无效,1代表有效
	char cAlValid;

	//天向速度 单位：m/s,向上为正，向下为负
	double fHVelocity;
	//0代表无效,1代表有效
	char cHVeValid;

	//真空速 单位：m/s
	double fTAS;
	//0代表无效,1代表有效
	char cTASValid;

	//修正高度 单位：m,向上修正为正，向下修正为负
	double fCorAltitude;
	//0代表无效,1代表有效
	char cCorValid;
}AIR_INFO_T;

//无线电高度
typedef struct RALT_INFO_T
{
	//时间 转换为ms
	unsigned long long ullTime;
	//0代表无效,1代表有效
	char cValid;

	//无线电高度 单位：m,海平面上为正，海平面下为负
	double fRALT;
	//0代表无效,1代表有效
	char cRALTValid;
}RALT_INFO_T;

//微波着陆
typedef struct ML_INFO_T
{
	//时间 转换为ms
	unsigned long long ullTime;
	//0代表无效,1代表有效
	char cValid;

	//下滑角偏差 单位：°，向上为正， 范围-18~18
	double fGlideBias;
	//0代表无效,1代表有效
	char cGliValid;

	//方位角偏差 单位：°，选择磁航线顺时针为正，范围[-120,120] 
	double fAzimuthBias;
	//0代表无效,1代表有效
	char cAzValid;
}ML_INFO_T;

//仪表着陆
typedef struct IL_INFO_T
{
	//时间 转换为ms
	unsigned long long ullTime;
	//0代表无效,1代表有效
	char cTValid;

	//下滑角偏差 单位：DDM，选择下滑道的上面为正，-10~10
	double fGlideBias;
	//0代表无效,1代表有效
	char cGliValid;

	//方位角偏差 单位：DDM，选择航线顺时针为正，-10~10
	double fAzimuthBias;
	//0代表无效,1代表有效
	char cAziValid;
}IL_INFO_T;

//前、左、右起落架
typedef enum LGP_INFO_U  //GEAR_INFO_U
{
	//缺省
	GEAR_NA,
	//放下,即在着陆状态
	GEAR_DN,
	//收起，即未在着陆状态
	GEAR_UP
} LGP_INFO_U;

//襟翼
typedef enum FLAP_INFO_U
{
	//缺省
	FLAP_NA,
	//放下,即在着陆状态
	FLAP_DN,
	//收起，即未在着陆状态
	FLAP_UP
}FLAP_INFO_U;

//轮载
typedef enum WHLD_INFO_U // WOW_INFO_U
{
//缺省
	WOW_NA,
	//地面
	WOW_GND,
	//空中
	WOW_AIR
} WHLD_INFO_U; //WOW_INFO_U;

//地平仪
typedef struct HORIZON_INFO_T
{
	//时间 转换为ms
	unsigned long long ullTime;
	//0代表无效,1代表有效
	char cTValid;

	//航向 单位：°，相对于真北顺时针为正，[0,360) 
	double fHDG;
	//0代表无效,1代表有效
	char cHDGValid;

	//横滚 单位：°，绕纵轴轴向顺时针转动为正，逆时针转动为负[-180～180]°
	double fRollAngle;
	//0代表无效,1代表有效
	char cRAValid;

	//俯仰 单位：°，水平面之上为正，水平面之下为负[-90～90]°
	double fPitchAngle;
	//0代表无效,1代表有效
	char cPAValid;
}HORIZON_INFO_T;

//油量
typedef struct OLMT_INFO_T
{
	//油量 单位：KG
	float fOIL;
	//0代表无效,1代表有效
	char cValid;
}OLMT_INFO_T;

//挂载
typedef struct MOUNT_INFO_T
{
	//(待定)
}MOUNT_INFO_T;

//量程
typedef enum RANGE_INFO_U
{
	RANGE_20,
	RANGE_40,
	RANGE_60,
	RANGE_80,
	RANGE_120,
	RANGE_160,
	RANGE_200,
	RANGE_240,
	RANGE_280,
	RANGE_320,
	RANGE_360,
	RANGE_400,
	RANGE_450,
	RANGE_DEFAULT
}RANGE_INFO_U;

/*
  字节	bit1~bit2	  bit3~bit4	    bit5~bit6	    bit7~bit8
  1	左控制板：0x00；  右控制板：0x01
  2	     按键1	      按键2	       按键3	       按键4
  3	     按键5	      按键6	       按键7	       按键8
  4	     按键9	      按键10	    按键11	       按键12
  5	     按键13   	   按键14	      按键15	       按键16
  6	指示灯1颜色状态	  指示灯1闪烁状态	  指示灯2颜色状态	指示灯2闪烁状态
  7	指示灯3颜色状态	  指示灯3闪烁状态	  指示灯4颜色状态	指示灯4闪烁状态
  8	指示灯5颜色状态	  指示灯5闪烁状态	  指示灯6颜色状态	指示灯6闪烁状态
  9	指示灯7颜色状态	  指示灯7闪烁状态	  指示灯8颜色状态	指示灯8闪烁状态
  10	指示灯9颜色状态	  指示灯9闪烁状态	  指示灯10颜色状态	指示灯10闪烁状态
  11	指示灯11颜色状态  指示灯11闪烁状态	  指示灯12颜色状态	指示灯12闪烁状态
  12	指示灯13颜色状态  指示灯13闪烁状态	  指示灯14颜色状态	指示灯14闪烁状态
  13	指示灯15颜色状态  指示灯15闪烁状态	  指示灯16颜色状态	指示灯16闪烁状态
  14	特殊功能按键(组合按键)
  15	表示控制哪个灯 按位读取，0表示不控制，1表示控制，2个字节16位，对应控制16个灯
  16	表示控制哪个灯
  说明：
  a)字节1，指令源，左控制板：0x00；  右控制板：0x01
  b)按键：01单次短按键状态；11单次长按键状态
  c)指示灯颜色：00表示灭；01表示绿色；10表示琥珀色；11表示红色
  d)指示灯状态：00表示不闪烁；01表示每0.5秒闪烁一次；10表示每1秒闪烁一次；11表示每1.5秒闪烁一次
  e) 第14字节，特殊功能按键表示组合按键，预留
  f)15到16字节共16bit，确定本次控制的灯有哪些或者哪些按键有响应，第15字节从低位到高位对应按键1~按键8或灯1~灯8；第16字节从低位到高位对应按键9~按键16或灯9~16；0表示不控制或无响应，1表示控制或有响应
*/
//近地告警控制板按键信息
typedef struct EGPWP_KEY_INFO_T
{
	char cEgpwpKey[16];
}EGPWP_KEY_INFO_T;

//数据管理状态切换命令
typedef enum DMD_STATUS_SWITCH_U
{
	//状态切换
	STATUS_SWITCH,
	//升级
	UPGRADE,
	//回退
	ROLLBACK,
	//导出(导出记录日志,待定)
	EXPORT,
	//导入任务数据
	IMPORT
}DMD_STATUS_SWITCH_U;

//地形板在线升级、回退、导出结果
typedef struct TW_UPBACK_T
{
	//0x00代表失败，0x01代表成功，0x11代表未改变
	//程序
	char cPro;
	//工作参数
	char cConfig;
	//地形数据库
	char cTWData;
	//机场数据库
	char cAirportData;
//任务数据
	char cTaskData;
//记录日志
	char cLog;
}TW_UPBACK_T;

//告警板在线升级、回退结果
typedef struct GW_UPBACK_T
{
	//0x00代表失败，0x01代表成功
	//程序
	char cPro;
	//工作参数
	char cConfig;
}GW_UPBACK_T;

//时间定义
typedef struct LOCAL_TIME_T
{
	//年
	unsigned int uYear;
//月
	unsigned int uMonth;
//日
	unsigned int uDay;
//毫秒（当天24h时间转换为ms）
	unsigned int uMs;
} LOCAL_TIME_T;


/**
 * @功能：获得告警处理板自检结果
 * @入参：无
 * @出参：pGwbScInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetGwbScInfo(GWB_SC_INFO_T* pGwbScInfo);

/**
 * @功能：获得地形处理板自检结果
 * @入参：无
 * @出参：pGwbScInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetTwbScInfo(TWB_SC_INFO_T* pTwbScInfo);

/**
 * @功能：获得近地告警左控制板自检结果
 * @入参：无
 * @出参：pGwbScInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetLeftEgpwpScInfo(EGPWP_SC_INFO_T* pEgpwpScInfo);

/**
 * @功能：获得近地告警右控制板自检结果
 * @入参：无
 * @出参：pGwbScInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetRightEgpwpScInfo(EGPWP_SC_INFO_T* pEgpwpScInfo);

/**
 * @功能：获得惯导数据信息
 * @入参：无
 * @出参：pInInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetInInfo(IN_INFO_T* pInInfo);

/**
 * @功能：获得大气测量数据信息
 * @入参：无
 * @出参：pAirInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetAirInfo(AIR_INFO_T* pAirInfo);

/**
 * @功能：获得无线电高度数据信息
 * @入参：无
 * @出参：pAirInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetRaltInfo(RALT_INFO_T* pRaltInfo);

/**
 * @功能：获得微波着陆数据信息
 * @入参：无
 * @出参：pMlInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetMlInfo(ML_INFO_T* pMlInfo);

/**
 * @功能：获得仪表着陆数据信息
 * @入参：无
 * @出参：pIlInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetIlInfo(IL_INFO_T* pIlInfo);

/**
 * @功能：获得前起落架数据信息
 * @入参：无
 * @出参：pLgpInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetFrontLgpInfo(LGP_INFO_U* pLgpInfo);

/**
 * @功能：获得左起落架数据信息
 * @入参：无
 * @出参：pLgpInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetLeftLgpInfo(LGP_INFO_U* pLgpInfo);

/**
 * @功能：获得右起落架数据信息
 * @入参：无
 * @出参：pLgpInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetRightLgpInfo(LGP_INFO_U* pLgpInfo);

/**
 * @功能：获得襟翼数据信息
 * @入参：无
 * @出参：pFlapInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetFlapInfo(FLAP_INFO_U* pFlapInfo);

/**
 * @功能：获得轮载数据信息
 * @入参：无
 * @出参：pWhldInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetWhldInfo(WHLD_INFO_U* pWhldInfo);

/**
 * @功能：获得地平仪数据信息
 * @入参：无
 * @出参：pHorizonInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetHorizonInfo(HORIZON_INFO_T* pHorizonInfo);

/**
 * @功能：获得油量数据信息
 * @入参：无
 * @出参：pOlmtInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetOlmtInfo(OLMT_INFO_T* pOlmtInfo);

/**
 * @功能：获得挂载数据信息
 * @入参：无
 * @出参：pMountInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetMountInfo(MOUNT_INFO_T* pMountInfo);

/**
 * @功能：获得量程数据信息
 * @入参：无
 * @出参：pRangeInfo
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetRangeInfo(RANGE_INFO_U* pRangeInfo);

typedef void(*KeyOpCallBackFuc)(EGPWP_KEY_INFO_T* ptKeyInfo);

/**
 * @功能：左、右近地告警控制板按键事件触发
 * @入参：pFuc，回调函数函数指针
 * @出参：无
 * @返回：无
 * @备注：
 */
void RegKeyOpCallBack(KeyOpCallBackFuc* pFunc);

/**
 * @功能：左、右近地告警控制板接收指示灯控制信息
 * @入参：tKeyInfo
 * @出参：无
 * @返回：无
 * @备注：0代表成功，-1代表失败
 */
int SendLightInfo(EGPWP_KEY_INFO_T tKeyInfo);

typedef void(*DmdBeforeCallBackFuc)(DMD_STATUS_SWITCH_U* uConsoleChange);

/**
 * @功能：数据管理设备升级、回退、导出、导入触发
 * @入参：pFunc，回调函数函数指针
 * @出参：无
 * @返回：无
 * @备注： 
 */
void RegBeforeDmdCallBack(DmdBeforeCallBackFuc* pFunc);

typedef void(*AfterDmdCallBackFuc)(TW_UPBACK_T* tTwUpbackInfo, GW_UPBACK_T* tGwUpbackInfo);

/**
 * @功能：数据管理设备升级、回退、导出、导入功能执行完成时触发
 * @入参：pFuc，回调函数函数指针
 * @出参：无
 * @返回：无
 * @备注： 
 */
void RegAfterDmdCallBack(AfterDmdCallBackFuc* pFuc);

/**
 * @功能：告警处理软件通知告警板进行升级、回退、导入、导出
 * @入参：uDmdStatusSwitch
 * @出参：无
 * @返回：无
 * @备注：
 */
void SendDmdStatusSwitch(DMD_STATUS_SWITCH_U uDmdStatusSwitch);

/**
 * @功能：获得当前时间
 * @入参：无
 * @出参：pLocalTime
 * @返回：0代表成功，-1代表失败
 * @备注：
 */
int GetLocalTime(LOCAL_TIME_T* pLocalTime);

/**
 * @功能：语音播放(暂定应用层实现)
 * @入参：pMp3File  pFile,mp3文件路径
 * @出参：无
 * @返回：
 * @备注：
 */
int VoicePlay(char* pMp3File);

#endif
