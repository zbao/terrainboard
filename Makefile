CC = gcc
CFLAGS = -c -pedantic -Wall -Wextra -I./
LDFLAGS = -lpthread
#LDFLAGS += -lglut -lGL -lGL -lm -lGLEW

SRC = $(wildcard *.c) $(wildcard *.cpp)
HEADER = $(wildcard *.h)
OBJ = $(SRC:%.c=%.o)

TARGET = terrainboard

all: $(SRC) $(TARGET)

$(TARGET): $(OBJ)
	$(CC) $(OBJ) $(LDFLAGS) -o $@

%.o: %.c $(HEADER)
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f $(OBJ) $(TARGET)

distclean:
	rm -f $(OBJ) $(TARGET) *~ \#*

.PHONY: clean distclean all
