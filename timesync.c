#include<stdio.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<unistd.h>
#include<arpa/inet.h>

#include "define.h"
#include "controlboard.h"

static    pthread_t    net_recv_threadid    = 0;
int sockfd = 0;
struct sockaddr_in addr;
LOCAL_TIME_T   local_time;
void*        net_recv_thread(void *param)
{
	struct sockaddr_in cli;
	socklen_t len=sizeof(cli);
#if 1
	while(1) {
//		char buf =0;
		CONTROL_BOARD_MESSAGE_TIME_T packet;
		recvfrom(sockfd,&packet,sizeof(packet), 0,(struct sockaddr*)&cli,&len);
//		printf("recv num =%hhd\n",buf);
		local_time.uYear = packet.year;
		local_time.uMonth = packet.month;
		local_time.uDay = packet.day;
		local_time.uMs  = 0;
		printf("packet =%d\n", packet.year);

//        buf =66;
//        sendto(sockfd,&buf,sizeof(buf),0,(struct sockaddr*)&cli,len);
	}
#endif

	pthread_exit(0);
	return NULL;
}
int init_network()
{
	//创建socket对象
	sockfd=socket(AF_INET, SOCK_DGRAM, 0);

	//创建网络通信对象
	addr.sin_family = AF_INET;
	addr.sin_port =htons(6200);
	addr.sin_addr.s_addr = inet_addr("127.0.0.1");//inet_addr("192.168.1.88");

	int ret =bind(sockfd,(struct sockaddr*)&addr,sizeof(addr));
	if(0>ret)
	{
		printf("bind\n");
		return -1;
	}

	pthread_create(&net_recv_threadid, NULL, net_recv_thread, NULL);
//	close(sockfd);
}
