#ifndef _CONTROLBOARD_H_
#define _CONTROLBOARD_H_

#define MESSAGE_MAGIC 0x5a5a5a5a

typedef struct _CONTROL_BOARD_MESSAGE_HEAD_T {
	unsigned int      magic; /* 0x5a5a5a5a */
	unsigned short    length;
	unsigned char     src_board;
	unsigned char     dst_board;
	unsigned char     msg;
	unsigned char     data[128];
} __attribute__((packed)) CONTROL_BOARD_MESSAGE_HEAD_T;

typedef struct _CONTROL_BOARD_MESSAGE_TIME_T {
	unsigned int	magic;
	unsigned short	year;
	unsigned char	month;
	unsigned char	day;
	unsigned char	hour;
	unsigned char	minute;
	unsigned char	second;
	unsigned int	msecond;
} __attribute__ ((packed)) CONTROL_BOARD_MESSAGE_TIME_T;

typedef struct _CONTROLBOARD_KEYS_T {
	CONTROL_BOARD_MESSAGE_HEAD_T head;
	unsigned char     key;
	unsigned char     key_combined[3];
} __attribute__((packed)) CONTROLBOARD_KEY_T;

/* Internal message */
#define MSG_SINGLE_KEY	0x0A
#define MSG_LONGPRESS_KEY	0x0B
#define MSG_MULTIPLE_KEY	0xC
#define MSG_KEY_RESP		0xD

#define MSG_LIGHT		0xE
#define MSG_LIGHT_RESP		0xF

#define MSG_FLASHDISK_PLUG	0x10
#define MSG_FLASHDISK_PLUG_RESP 0x11

#define MSG_FLASHDISK_UNPLUG	0x12
#define MSG_FLASHDISK_UNPLUG_RESP 0x13

#define MSG_FILENAME            0x14
#define MSG_FILENAME_RESP	0x15

#define MSG_FILE_DATA		0x16
#define MSG_FILE_DATA_LAST	0x17

#define MSG_FILE_DATA_RESP	0x18

#endif
